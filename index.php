<!DOCTYPE html>
<html lang="de">
<head>
    <title>Currency converter</title>
    <link rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.11.2/css/all.css"
          integrity="sha384-KA6wR/X5RY4zFAHpv/CnoG2UW1uogYfdnP67Uv7eULvTveboZJg0qUpmJZb5VqzN"
          crossorigin="anonymous">
    <link rel="stylesheet"
          type="text/css"
          href="ui/css/main.css">
</head>
<body>
    <?php if (isset($_GET['config'])) {
        include_once 'app/config/template.php';
    } else {
        include_once 'app/converter/template.php';
    } ?>
</body>
</html>