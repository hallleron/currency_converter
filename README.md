## Currency converter sample exercise

This micro application is able to convert a certain money amount to a new currency.
___
#### Language:

The main language used is PHP, however some basic JavaScript has been used in 
order to keep the frontend GUI interactive.
___
#### Design choice:

Since the application is extremely small, I chose to avoid an MVC setup to keep 
the overhead as small as possible. So instead I went with a functional paradigm.

#### Functionality

It is possible to adjust the conversion rates by clicking the gears icon in the 
top right. Each individual rate can then be adjusted and will be saved permanently 
using the file system as a persistent storage. A database integration has been avoided 
because it is not necessary and more or less overkill. The data is stored in a JSON file 
on the file system.