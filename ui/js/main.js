/**
 * DOM elements
 * @type {HTMLElement}
 */
let amount = document.getElementById('amount');
let convert_from = document.getElementById('convert_from');
let convert_to = document.getElementById('convert_to');
let result = document.getElementById('result');

/**
 * Adds the process event handler
 * @type {NodeListOf<Element>}
 */
let elements = document.querySelectorAll('.conversion_trigger');
for (let i = 0; i < elements.length; i++) {
    elements[i].addEventListener('input', process);
}

/**
 * @param event
 */
function process (event) {
    if (event.target.id === 'convert_from') {
        setSelectOptions();
    }

    if (requiredValuesExist()) {
        calculateViaAjax();
    } else {
        result.textContent = '';
    }
}

/**
 * Assign the matching options for the destination currency
 * @param value
 */
function setSelectOptions (value = null) {

    let rates = {};
    if (value === null) {
        rates = exchangeRates[event.target.value];
    } else {
        rates = exchangeRates[value];
    }

    convert_to.options.length = 0;

    for (let label in rates) {
        if (rates.hasOwnProperty(label)) {
            convert_to.options[convert_to.options.length] = new Option(label, rates[label]);
        }
    }
}

/**
 * Ajax call to calculate the desired rate
 */
function calculateViaAjax () {
    let xhr = new XMLHttpRequest();
    xhr.open('post', '/app/converter/calculator.php');
    xhr.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');

    xhr.onreadystatechange = function() {
        if (this.readyState === XMLHttpRequest.DONE && this.status === 200) {
            result.textContent = this.response;
        }
    };

    xhr.send('amount='+amount.value+'&to='+convert_to.value);
}

/**
 * Check to see if all information needed for calculation are present
 * @returns {boolean}
 */
function requiredValuesExist () {
    let exist = true;

    if (amount.value === '') {
        exist = false;
    }

    if (convert_from.value === '') {
        exist = false;
    }

    if (convert_to.value === '') {
        exist = false;
    }

    return exist;
}

/**
 * Initialize the select options
 */
setSelectOptions(convert_from.value);