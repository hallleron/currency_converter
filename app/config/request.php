<?php

require_once 'storage.php';

if (isset($_POST['submit'])) {
    unset($_POST['submit']);

    saveExchangeRates($_POST);
}