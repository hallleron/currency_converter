<?php

define('STORAGE_FILE', __DIR__.'/../../storage/exchange_rates.json');

function getExchangeRates()
{
    $json = file_get_contents(STORAGE_FILE);

    if ($json === false) {
        return null;
    }

    return json_decode($json, true);
}

function saveExchangeRates($rates)
{
    $jsonRates = [];
    foreach ($rates as $currencies => $rate) {
        $currencies = explode('-', $currencies);
        $fromCurrency = $currencies[0];
        $toCurrency = $currencies[1];

        $jsonRates[$fromCurrency][$toCurrency] = $rate;
    }
    $json = json_encode($jsonRates, JSON_PRETTY_PRINT);

    file_put_contents(STORAGE_FILE, $json);

    header("Location: /");

    die();
}