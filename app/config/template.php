<?php require_once 'app/config/storage.php'; ?>

<div id="wrapper"
     class="center border-white">
    <form action="app/config/request.php"
          method="post"
          id="config_form">
        <?php $data = getExchangeRates(); ?>
        <table>
            <thead>
                <th>Wechselkurs</th>
                <th>Umrechnungsfaktor</th>
            </thead>
            <tbody>
            <?php foreach ($data as $fromLabel => $rates): ?>
                <?php foreach ($rates as $toLabel => $rate): ?>
                    <?php $id = $fromLabel.'-'.$toLabel; ?>
                    <tr>
                        <td>
                        <label for="<?= $id; ?>">
                            <?= $fromLabel; ?> zu <?= $toLabel; ?>
                        </label>
                        </td>
                        <td>
                        <input type="text"
                               name="<?= $id; ?>"
                               id="<?= $id; ?>"
                               value="<?= $rate; ?>"/>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endforeach; ?>
            </tbody>
        </table>
        <button type="submit"
                name="submit">
            <i class="fas fa-save"></i>
            Speichern und zurückkehren
        </button>
    </form>
</div>