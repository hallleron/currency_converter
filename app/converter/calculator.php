<?php

require_once '../config/storage.php';

if (!isset($_POST['amount'])) {
    die();
}

$factor = floatval($_POST['to']);
$amount = formatAmount($_POST['amount']);

$result = $amount * $factor;
die(number_format((float)$result, 2, ',', '.'));

function formatAmount($amount)
{
    $amount = str_replace('.', '', $amount);
    $amount = str_replace(',', '.', $amount);

    return floatval($amount);
}