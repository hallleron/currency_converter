<?php require_once 'app/config/storage.php'; ?>

<header>
    <a class="config_link" href="/?config">
        <i class="fas fa-cogs"></i>
    </a>
</header>

<div id="wrapper"
     class="center border-white">
    <?php $data = getExchangeRates(); ?>
    <form>
        <div class="source">
            <input type="text"
                   name="amount"
                   class="conversion_trigger"
                   id="amount" />

            <select name="convert_from"
                    class="conversion_trigger"
                    id="convert_from">
                <?php foreach ($data as $fromLabel => $rate): ?>
                    <option value="<?= $fromLabel; ?>">
                        <?= $fromLabel; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>

        <span class="arrow">
            <img src="/ui/img/arrow.svg"
                 id="arrow_icon"/>
        </span>


        <div class="destination">
            <select name="convert_to"
                    class="conversion_trigger"
                    id="convert_to">
            </select>
        </div>

        <span id="result"></span>
    </form>
</div>

<script type="application/javascript">
    window.exchangeRates = JSON.parse('<?= json_encode($data); ?>');
</script>
<script type="application/javascript" src="/ui/js/main.js"></script>